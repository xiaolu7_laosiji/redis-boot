package com.lagou.cluster;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RedisWebApp {
    public static void main(String[] args) {
        SpringApplication.run(RedisWebApp.class, args);
    }
}
