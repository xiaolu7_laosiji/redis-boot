# redis集群搭建

## 集群规划

| ip                | 服务  | 角色 |
| ----------------- | ----- | ---- |
| 192.168.3.24:6379 | redis | 集群 |
| 192.168.3.24:6389 | redis | 集群 |
| 192.168.3.7:6379  | redis | 集群 |
| 192.168.3.7:6389  | redis | 集群 |
| 192.168.3.3:6379  | redis | 集群 |
| 192.168.3.3:6389  | redis | 集群 |

## 安装步骤

首先，我们要在第一台虚拟机上，编译安装redis，执行以下命令：

```shell
cd /opt/soft
wget http://download.redis.io/releases/redis-5.0.8.tar.gz
tar -xzvf redis-5.0.8.tar.gz
mkdir redis5
cd /opt/soft/redis-5.0.8
make  install PREFIX=/opt/soft/redis5
cp /opt/soft/redis-5.0.8/redis.conf /opt/soft/redis5
```

执行上面步骤后，编辑redis.conf

**vi /opt/soft/redis5/redis.conf**

```
#注意一定要修改这里
cluster-enabled yes
# ip要修改
bind 192.168.3.24
#后台运行
daemonize yes
#建议开启aof
appendonly yes
appendfsync everysec
```

注意，编译后的redis目录，可以直接复制到其他目录去，我们执行以下操作：

```shell
cp /opt/soft/redis5 /opt/soft/redis5-slave
#分发redis到其他虚拟机
scp -r redis5 root@192.168.3.7:/opt/soft
scp -r redis5 root@192.168.3.3:/opt/soft
```

执行完上面步骤后，先确认一下各台虚拟机的目录结构：

**虚拟机1**

ls -lh /opt/soft

```
drwxr-xr-x.  3 root root      91 7月  22 19:20 redis5
drwxrwxr-x.  6 root root    4.0K 7月  21 22:55 redis-5.0.8
-rw-r--r--.  1 root root    1.9M 6月  27 23:51 redis-5.0.8.tar.gz
drwxr-xr-x.  3 root root      91 7月  22 19:16 redis5-slave
```

**虚拟机2**

ls -lh /opt/soft

同虚拟机1

**虚拟机3**

ls -lh /opt/soft

同虚拟机1

确认好目录之后，启动各台虚拟机上的redis，并使用ps -ef |grep redis，得到以下输出：

```
root      3021     1  0 19:11 ?        00:00:01 bin/redis-server 192.168.3.24:6379 [cluster]
root      3026     1  0 19:11 ?        00:00:01 bin/redis-server 192.168.3.24:6389 [cluster]
```

之后，我们使用命令创建集群：

./redis-cli --cluster create 192.168.3.24:6379 192.168.3.3:6379 192.168.3.7:6379 

192.168.3.24:6389 192.168.3.7:6389 192.168.3.3:6389 --cluster-replicas 1

注意以上命令，尽量格式化为一行后去执行。

之后，我们可以使用redisson连接上redis做下测试，由于代码上传到了git，下面不做演示了。

## 扩容

我们可以在两台虚拟机上，再部署出来2台redis做扩容测试，步骤如下：

```shell
mkdir /opt/soft/redis5-7009
cd /opt/soft/redis-5.0.8
make  install PREFIX=/opt/soft/redis5-7009
#需要修改redis.conf，改端口为7009
./redis-cli --cluster add-node 192.168.3.24:7009 192.168.3.24:6379
./redis-cli --cluster add-node 192.168.3.7:7009 192.168.3.24:7009 --
cluster-slave --cluster-master-id 6ff20bf463c954e977b213f0e36f3efc02bd53d6
./redis-cli --cluster reshard 192.168.3.24:7009
```

注意，扩容时，是要先add-node之后再reshard，这样不容易出错

知识点扩充：

在学习过程中，我们知道

gossip协议包含多种消息，包括meet、ping、pong、fail、publish等等。

因此，也可以登录redis-cli客户端，使用以下命令加入node

cluster meet 192.168.3.24 7009

# 项目演示

运行**com.lagou.cluster.RedisWebApp**启动项目，在启动过程中，会出现以下打印：

```
78e4aa76411d918ab946cdcd62da1ec7a3a98615 192.168.3.7:6379@16379 master - 0 1595417496389 11 connected 316-455 6462-10922 11923-12194
5fd6e66012ec9449690d03d8679e25b42795d53f 192.168.3.24:6389@16389 slave 95171a429020f043995f8d85aa9c1f10e1835ce5 0 1595417498400 4 connected
7c62498efafd0e7578206f14b4ceef4b880b581e 192.168.3.7:6389@16389 slave 7f9258645c27ff19a890f8d7e9a5f61d5a2eae30 0 1595417496000 9 connected
7f9258645c27ff19a890f8d7e9a5f61d5a2eae30 192.168.3.24:6379@16379 myself,master - 0 1595417495000 9 connected 884-6461 10923-11922
1c54e8f1072901cd8beff7a812d200f17c01deee 192.168.3.24:7009@17009 master - 0 1595417499403 12 connected 0-315 456-883 12195-12449
55ade12cfdd0458f63409c07025ddc2b256d1f1f 192.168.3.3:6389@16389 slave 78e4aa76411d918ab946cdcd62da1ec7a3a98615 0 1595417500411 11 connected
ee1849d40146278dcb0f51ddc86aa4d46fe41cbc 192.168.3.7:7009@17009 slave 1c54e8f1072901cd8beff7a812d200f17c01deee 0 1595417498000 12 connected
95171a429020f043995f8d85aa9c1f10e1835ce5 192.168.3.3:6379@16379 master - 0 1595417497000 3 connected 12450-16383
```

在application.yaml中，只需要配置以下一个地址：

```
org:
  redisson:
    nodes: 192.168.3.24:6379
```

redisson会自动处理集群对应关系，不需要写全所有ip，原理就是redisson启动时，会使用cluster nodes命令。

访问以下地址进行批量key录入

http://127.0.0.1:8080/example/batch

登录虚拟机查看key分布。

```shell
bin/redis-cli -h 192.168.3.24 info keyspace
#Keyspace
db0:keys=393,expires=0,avg_ttl=0
```

